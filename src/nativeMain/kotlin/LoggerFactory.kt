package io.skerna.common.logger

import kotlin.native.concurrent.ThreadLocal
import kotlin.reflect.KClass

@ThreadLocal
actual object LoggerFactory {

    private var delegateFactory: LogDelegateFactory? = null
    private var loggers = mutableMapOf<String, Logger>()

    init {
        initialise()
    }

    actual fun initialise() {
        delegateFactory = PrintDelegateFactory()

    }

    actual inline fun logger(clazz: KClass<*>): Logger {
        return logger(clazz.simpleName!!)
    }

    actual fun logger(name: String): Logger {
        return logger(name,LoggerConfiguration.instanceGlobalContext)
    }

    /**
     * returns new logger with specific configuration
     * if logger doest not exists a new logger is created using name and configuration
     * pass as parameters
     * @return Logger
     */
    actual fun logger(name: String, configuration: LoggerConfiguration): Logger {
        var logger: Logger? = loggers[name]

        if (logger == null) {
            val delegate = delegateFactory!!.createDelegate(name,configuration)

            logger = Logger(delegate)

            val oldLogger = loggers.put(name, logger)

            if (oldLogger != null) {
                logger = oldLogger
            }
        }

        return logger
    }

    actual fun removeLogger(name: String) {
        loggers.remove(name)
    }

    actual fun setLogDelegateFactory(delegateFactory: LogDelegateFactory) {
        LoggerFactory.delegateFactory = delegateFactory
    }

    actual inline fun <reified T> logger(): Logger {
        return logger(T::class)
    }




}
