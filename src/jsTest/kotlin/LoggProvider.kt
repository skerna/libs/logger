package io.skerna.common.logger

actual object LoggProvider {
    actual fun getLoggersFactories(): Array<LogDelegateFactory> {
        return arrayOf<LogDelegateFactory>()
    }
}
