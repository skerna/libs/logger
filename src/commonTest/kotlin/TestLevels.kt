package io.skerna.common.logger

import kotlin.test.Test
import kotlin.time.ExperimentalTime
import kotlin.time.measureTime

class TestLevels {
    val logger:Logger
    init{
        LoggerConfiguration.instanceGlobalContext.enableAll()
        logger = LoggerFactory.logger<TestLevels>();
    }

    private fun getLoggers(name:String):List<Logger>{
        return LoggProvider.getLoggersFactories().map {
            val delegate  = it.createDelegate(name, LoggerConfiguration.instanceGlobalContext)
            val logger = Logger(delegate)
            logger
        }
    }
    @ExperimentalTime
    @Test
    fun testInfo(){
        getLoggers("test").forEach { logger ->
            val time = measureTime { logger.atInfo().log("Info Test {}","ronald") }
            println("Time -> " + time)
        }
    }
    @ExperimentalTime
    @Test
    fun testDebug(){
        getLoggers("test").forEach { logger ->
            val time = measureTime { logger.atDebug().log("Debug Test {}","ronald") }
            println("Time -> " + time)
        }
    }
    @ExperimentalTime
    @Test
    fun testWarn(){
        getLoggers("test").forEach { logger ->
            val time = measureTime { logger.atWarning().log("Warn Test {}","ronald") }
            println("Time -> " + time)
        }
    }
    @ExperimentalTime
    @Test
    fun testTrace(){
        getLoggers("test").forEach { logger ->
            val time = measureTime { logger.atError().log("Trace Test {}","ronald") }
            println("Time -> " + time)
        }
    }
    @ExperimentalTime
    @Test
    fun testError() {
        getLoggers("test").forEach { logger ->
            val time = measureTime { logger.atError { log("test") } }
            println("Time -> " + time)
        }
    }
}
