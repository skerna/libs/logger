package io.skerna.common.logger

expect object LoggProvider{
    fun getLoggersFactories():Array<LogDelegateFactory>
}
