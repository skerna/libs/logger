package io.skerna.common.logger

import org.junit.Test

class TestSlf4j {
    @Test
    fun `expected support several arguments`() {
        val debugStatus = false
        val warnStatus = true
        LoggerFactory.setLogDelegateFactory(Log4j2LogDelegateFactory())
        val log4j2 = Log4j2LogDelegate("name", LoggerConfiguration.instanceGlobalContext)
        log4j2.info("Hola {} {} {} {}",1,2,3,"Ronald")
    }

    @Test
    fun `expected support Slf4 several arguments`() {
        val debugStatus = false
        val warnStatus = true
        LoggerFactory.setLogDelegateFactory(SLF4JLogDelegateFactory())
        val log4j2 = LoggerFactory.logger("test")
        log4j2.atInfo().log("Hola {} {} {} {}","tst" , 1,2,3,"Ronald")
    }
}
