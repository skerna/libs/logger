

package io.skerna.common.logger


/**
 * @author Ronald Cárdenas
 **/
class ALogDelegateFactory:LogDelegateFactory{
    override fun createDelegate(name: String, configuration: LoggerConfiguration): LogDelegate {
        return ALogDelegte(name,configuration)
    }
}
