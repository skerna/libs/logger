

package io.skerna.common.logger

import com.rexcode.console.colors.yellow
import java.util.concurrent.ConcurrentHashMap
import kotlin.reflect.KClass

actual object LoggerFactory {

    @Volatile
    private var delegateFactory: LogDelegateFactory? = null

    private val loggers = ConcurrentHashMap<String, Logger>()

    init {
        initialise()
    }

    @Synchronized
    actual fun initialise() {
        val delegateFactory: LogDelegateFactory

        // If a system property is specified then this overrides any delegate factory which is set
        // programmatically - this is primarily of use so we can configure the logger delegate on the client side.
        // call to System.getProperty is wrapped in a try block as it will fail if the client runs in a secured
        // environment
        var className: String? = ALogDelegte::class.java.name
        try {
            className = System.getProperty(LOGGER_DELEGATE_FACTORY_CLASS_NAME)
        } catch (e: Exception) {
            println("Warning: se trato de acceder a la propiedad $LOGGER_DELEGATE_FACTORY_CLASS_NAME  ${e.message}".yellow())
        }
        if (className != null) {
            val loader = Thread.currentThread().contextClassLoader
            //val loader = this.javaClass.classLoader
            try {
                val clz = loader.loadClass(className)
                delegateFactory = clz.getConstructor().newInstance() as LogDelegateFactory
            } catch (e: Exception) {
                throw IllegalArgumentException("Errr instantiating transformer class \"$className\"", e)
            }

        } else {
            delegateFactory = ALogDelegateFactory()
        }
        configureTargetLogger(delegateFactory)
        LoggerFactory.delegateFactory = delegateFactory
    }

    /**
     * set custom logger deletage factory
     * @param delegateFactory
     */
    private fun configureTargetLogger(delegateFactory: LogDelegateFactory) {
        this.delegateFactory = delegateFactory
    }

    @JvmStatic
    actual inline fun logger(clazz: KClass<*>): Logger {
        val javaClass = clazz.java
        return logger(javaClass)
    }

    @JvmStatic
    fun logger(clazz: Class<*>): Logger {
        val name = if (javaClass.isMemberClass)
            javaClass.enclosingClass!!.canonicalName
        else
            clazz.canonicalName
        return logger(name!!)
    }

    @JvmStatic
    actual fun logger(name: String): Logger {
        return logger(name, LoggerConfiguration.instanceGlobalContext)
    }
    /**
     * returns new logger with specific configuration
     * if logger doest not exists a new logger is created using name and configuration
     * pass as parameters
     * @return Logger
     */
    actual fun logger(name: String, configuration: LoggerConfiguration): Logger {
        var logger: Logger? = loggers[name]

        if (logger == null) {
            val delegate = delegateFactory!!.createDelegate(name,LoggerConfiguration.instanceGlobalContext)

            logger = Logger(delegate)

            val oldLogger = (loggers).putIfAbsent(name, logger)

            if (oldLogger != null) {
                logger = oldLogger
            }
        }
        return logger
    }



    @JvmStatic
    actual fun removeLogger(name: String) {
        loggers.remove(name)
    }



    @JvmStatic
    actual fun setLogDelegateFactory(delegateFactory: LogDelegateFactory) {
        //  configureTargetLogger(delegateFactory)
        System.setProperty(LOGGER_DELEGATE_FACTORY_CLASS_NAME,delegateFactory.javaClass.canonicalName)
        initialise()
    }

    actual inline fun <reified T> logger(): Logger {
        return logger(T::class)
    }



}
