

package io.skerna.common.logger


import org.apache.logging.log4j.Level
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.spi.ExtendedLogger
import org.apache.logging.log4j.spi.ExtendedLoggerWrapper


/**
 * A [LogDelegate] which delegates to Apache Log4j 2
 *
 */
class Log4j2LogDelegate internal constructor(name: String,
                                             configuration: LoggerConfiguration) : AbstractLoggerDelegate(name, configuration) {

    internal val logger:ExtendedLoggerWrapper

    override val isWarnEnabled: Boolean
        get() = logger.isWarnEnabled || LoggerContext.isWarnEnabled()

    override val isInfoEnabled: Boolean
        get() = logger.isInfoEnabled || LoggerContext.isInfoEnabled()

    override val isDebugEnabled: Boolean
        get() = logger.isDebugEnabled || LoggerContext.isDebugEnabled()

    override val isTraceEnabled: Boolean
        get() = logger.isTraceEnabled || LoggerContext.isTraceEnabled()

    init {
        configurateDefaultLogger()
        val _logger = LogManager.getLogger(name) as ExtendedLogger
        logger = ExtendedLoggerWrapper(_logger,name,_logger.getMessageFactory())
    }

    private fun configurateDefaultLogger() {

    }

    override fun fatal(message: Any) {
        log(Level.FATAL, message.toString())
    }

    override fun fatal(message: Any, t: Throwable) {
        log(Level.FATAL, message.toString(), t)
    }

    override fun error(message: Any) {
        log(Level.ERROR, message.toString())
    }

    override fun error(message: Any, vararg params: Any) {
        log(Level.ERROR, message.toString(), *params)
    }

    override fun error(message: Any, t: Throwable) {
        log(Level.ERROR, message.toString(), t)
    }

    override fun error(message: Any, t: Throwable, vararg params: Any) {
        log(Level.ERROR, message.toString(), t, *params)
    }

    override fun warn(message: Any) {
        log(Level.WARN, message.toString())
    }

    override fun warn(message: Any, vararg params: Any) {
        log(Level.WARN, message.toString(), *params)
    }

    override fun warn(message: Any, t: Throwable) {
        log(Level.WARN, message.toString(), t)
    }

    override fun warn(message: Any, t: Throwable, vararg params: Any) {
        log(Level.WARN, message.toString(), t, *params)
    }

    override fun info(message: Any) {
        logger.info(message)
    }

    override fun info(message: Any, vararg params: Any) {
        log(Level.INFO, message.toString(), *params)
    }

    override fun info(message: Any, t: Throwable) {
        log(Level.INFO, message.toString(), t)
    }

    override fun info(message: Any, t: Throwable, vararg params: Any) {
        log(Level.INFO, message.toString(), t, *params)
    }

    override fun debug(message: Any) {
        log(Level.DEBUG, message.toString())
    }

    override fun debug(message: Any, vararg params: Any) {
        log(Level.DEBUG, message.toString(), *params)
    }

    override fun debug(message: Any, t: Throwable) {
        log(Level.DEBUG, message.toString(), t)
    }

    override fun debug(message: Any, t: Throwable, vararg params: Any) {
        log(Level.DEBUG, message.toString(), t, *params)
    }

    override fun trace(message: Any) {
        log(Level.TRACE, message.toString())
    }

    override fun trace(message: Any, vararg params: Any) {
        log(Level.TRACE, message.toString(), *params)
    }

    override fun trace(message: Any, t: Throwable) {
        log(Level.TRACE, message.toString(), t)
    }

    override fun trace(message: Any, t: Throwable, vararg params: Any) {
        log(Level.TRACE, message.toString(), t, *params)
    }

    private fun log(level: Level, message: String, t: Throwable? = null) {
        logger.log(level,message,t)
    }

    private fun log(level: Level, message: String, vararg params: Any) {
        logger.log(level,message,*params)
    }

    private fun log(level: Level, message: String, t: Throwable, vararg params: Any) {
        logger.log(level,message,*params,t)
    }

    override fun unwrap(): Any {
        return logger
    }


    companion object {
        internal val FQCN = Logger::class.java.canonicalName
    }


}
