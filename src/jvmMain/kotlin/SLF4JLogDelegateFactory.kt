

package io.skerna.common.logger


class SLF4JLogDelegateFactory : LogDelegateFactory {
    override fun createDelegate(name: String,configuration: LoggerConfiguration): SLF4JLogDelegate {
        return SLF4JLogDelegate(name,configuration)
    }

}
