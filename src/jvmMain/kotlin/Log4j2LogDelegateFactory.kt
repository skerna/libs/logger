

package io.skerna.common.logger

class Log4j2LogDelegateFactory : LogDelegateFactory {
    override fun createDelegate(name: String,configuration: LoggerConfiguration): LogDelegate {
        return Log4j2LogDelegate(name,configuration)
    }
}
