

package io.skerna.common.logger


class VertxLogDelegateFactory : LogDelegateFactory {
    override fun createDelegate(name: String,configuration: LoggerConfiguration): LogDelegate {
        return VertxLoggerDelegate(name,configuration)
    }

}
