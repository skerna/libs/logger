

package io.skerna.common.logger


import io.skerna.common.logger.LogDelegate
import io.vertx.core.logging.LoggerFactory
import io.vertx.core.logging.Logger as VLogger

class VertxLoggerDelegate internal constructor(name: String,
                                               configuration: LoggerConfiguration) : AbstractLoggerDelegate(name,configuration) {

    private val logger: VLogger

    override val isWarnEnabled: Boolean
        get() = logger.isWarnEnabled

    override val isInfoEnabled: Boolean
        get() = logger.isInfoEnabled

    override val isDebugEnabled: Boolean
        get() = logger.isDebugEnabled

    override val isTraceEnabled: Boolean
        get() = logger.isTraceEnabled

    init {
        logger = LoggerFactory.getLogger(name)
    }

    override fun fatal(message: Any) {
        logger.fatal(message)
    }

    override fun fatal(message: Any, t: Throwable) {
        logger.fatal(message, t)
    }

    override fun error(message: Any) {
        logger.error(message)
    }

    override fun error(message: Any, vararg params: Any) {
        logger.error(message, *params)
    }

    override fun error(message: Any, t: Throwable) {
        logger.error(message, t)
    }

    override fun error(message: Any, t: Throwable, vararg params: Any) {
        logger.error(message, t, *params)
    }

    override fun warn(message: Any) {
        logger.warn(message)
    }

    override fun warn(message: Any, vararg params: Any) {
        logger.warn(message, *params)
    }

    override fun warn(message: Any, t: Throwable) {
        logger.warn(message, t)
    }

    override fun warn(message: Any, t: Throwable, vararg params: Any) {
        logger.warn(message, t, *params)
    }

    override fun info(message: Any) {
        logger.info(message)
    }

    override fun info(message: Any, vararg params: Any) {
        logger.info(message, *params)
    }

    override fun info(message: Any, t: Throwable) {
        logger.info(message, t)
    }

    override fun info(message: Any, t: Throwable, vararg params: Any) {
        logger.info(message, t, *params)
    }

    override fun debug(message: Any) {
        logger.debug(message)
    }

    override fun debug(message: Any, vararg params: Any) {
        logger.debug(message, *params)
    }

    override fun debug(message: Any, t: Throwable) {
        logger.debug(message, t)
    }

    override fun debug(message: Any, t: Throwable, vararg params: Any) {
        logger.debug(message, t, *params)
    }

    override fun trace(message: Any) {
        logger.trace(message)
    }

    override fun trace(message: Any, vararg params: Any) {
        logger.trace(message, *params)

    }

    override fun trace(message: Any, t: Throwable) {
        logger.trace(message, t)
    }

    override fun trace(message: Any, t: Throwable, vararg params: Any) {
        logger.trace(message, t, *params)
    }

    override fun unwrap(): Any {
        return logger
    }

}
