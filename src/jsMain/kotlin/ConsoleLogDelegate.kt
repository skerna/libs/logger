package io.skerna.common.logger

import io.skerna.common.logger.LogDelegate
import io.skerna.common.logger.LoggerContext

class ConsoleLogDelegate constructor(name: String,
                                     configuration: LoggerConfiguration) : AbstractLoggerDelegate(name,configuration) {
    override val isWarnEnabled: Boolean
        get() = LoggerContext.isWarnEnabled()
    override val isInfoEnabled: Boolean
        get() = LoggerContext.isInfoEnabled()
    override val isDebugEnabled: Boolean
        get() = LoggerContext.isDebugEnabled()
    override val isTraceEnabled: Boolean
        get() = LoggerContext.isTraceEnabled()

    override fun fatal(message: Any) {
        console.error(formatMessage(message))
    }

    override fun fatal(message: Any, t: Throwable) {
        console.error(formatMessage(message), t)
    }

    override fun error(message: Any) {
        console.error(formatMessage(message))
    }

    override fun error(message: Any, vararg params: Any) {
        console.error(formatMessage(message), params)
    }

    override fun error(message: Any, t: Throwable) {
        console.error(formatMessage(message), t)
    }

    override fun error(message: Any, t: Throwable, vararg params: Any) {
        console.error(formatMessage(message), t, params)
    }

    override fun warn(message: Any) {
        console.warn(formatMessage(message))
    }

    override fun warn(message: Any, vararg params: Any) {
        console.warn(formatMessage(message), params)
    }

    override fun warn(message: Any, t: Throwable) {
        console.warn(formatMessage(message), t)
    }

    override fun warn(message: Any, t: Throwable, vararg params: Any) {
        console.warn(formatMessage(message), t, params)
    }

    override fun info(message: Any) {
        console.info(formatMessage(message))
    }

    override fun info(message: Any, vararg params: Any) {
        console.info(formatMessage(message), params)
    }

    override fun info(message: Any, t: Throwable) {
        console.info(formatMessage(message), t)
    }

    override fun info(message: Any, t: Throwable, vararg params: Any) {
        console.info(formatMessage(message), t, params)
    }

    override fun debug(message: Any) {
        console.log(formatMessage(message))
    }

    override fun debug(message: Any, vararg params: Any) {
        console.log(formatMessage(message), params)
    }

    override fun debug(message: Any, t: Throwable) {
        console.log(formatMessage(message), t)
    }

    override fun debug(message: Any, t: Throwable, vararg params: Any) {
        console.log(formatMessage(message), t, message)
    }

    override fun trace(message: Any) {
        console.log(formatMessage(message))
    }

    override fun trace(message: Any, vararg params: Any) {
        console.log(formatMessage(message), params)
    }

    override fun trace(message: Any, t: Throwable) {
        console.log(formatMessage(message, t), t)
    }

    override fun trace(message: Any, t: Throwable, vararg params: Any) {
        console.log(formatMessage(message, t), t, params)
    }

    private fun formatMessage(message: Any): String {
        return "${this.name} : $message"
    }

    private fun formatMessage(message: Any, t: Throwable): String {
        return "${this.name} : $message, cause $t"
    }

    override fun unwrap(): Any {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
