package io.skerna.common.logger

import kotlin.native.concurrent.ThreadLocal
import kotlin.reflect.KClass

/**
 * Logger LoggerFactory, esta clase nos permite crear un nuevo logger
 * usando
 */
@ThreadLocal
expect object LoggerFactory {

    fun initialise()

    /**
     * Retorna un nuevo logger para una clase X
     * @param clazz
     * @return Logger
     */
    inline fun logger(clazz: KClass<*>): Logger

    /**
     * retorna un nuevo logger para un nombre x
     * @param name
     * @return Logger
     */
    fun logger(name: String): Logger

    /**
 * returns new logger with specific configuration
     * if logger doest not exists a new logger is created using name and configuration
     * pass as parameters
     * @return Logger
     */
    fun logger(name:String, configuration: LoggerConfiguration):Logger

    inline fun <reified T> logger(): Logger

    /**
     * Elimina un Logger del instance Holder
     * @param name
     */
    fun removeLogger(name: String)

    /**
     * Configura programaticamente el delegate Factory para crear nuevos loggers
     * @param delegateFactory
     */
    fun setLogDelegateFactory(delegateFactory: LogDelegateFactory)
}
