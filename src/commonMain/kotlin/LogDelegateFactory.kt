package io.skerna.common.logger

interface LogDelegateFactory {
    fun createDelegate(name: String,configuration: LoggerConfiguration): LogDelegate
}
