package io.skerna.common.logger

class PrintDelegateFactory : LogDelegateFactory {
    override fun createDelegate(name: String,configuration: LoggerConfiguration): LogDelegate {
        return PrintLogDelegate(name,configuration)
    }
}
