package io.skerna.common.logger

import kotlin.native.concurrent.ThreadLocal

/**
 * Base class for the fluent logger API. This class is a factory for instances of a fluent logging
 * API, used to build log statements via method chaining.
 *
 * @param <API> the logging API provided by this logger.
</API> */
abstract class AbstractLogger<API : LoggingApi<API>>  protected constructor(internal val backend: LogDelegate) {
    /**
     * Returns the non-null name of this logger (Flogger does not currently support anonymous
     * loggers).
     */
    protected val name: String
        get() = backend.getLoggerName()


    // ---- PUBLIC API ----

    /**
     * Returns a fluent logging API appropriate for the specified log level.
     *
     *
     * If a logger implementation determines that logging is definitely disabled at this point then
     * this method is expected to return a "no-op" implementation of the logging API, which will
     * result in all further calls made for the log statement to being silently ignored.
     *
     *
     * Typically an implementation of this method in a concrete subclass would look like:
     * <pre>`return isLoggable(level) ? new Context(level): NO_OP;
    `</pre> *
     * where `NO_OP` is a singleton, no-op instance of the logging API.
     */
    abstract fun at(level: Level): API

    /** A convenience method for at([Level.SEVERE]).  */
    fun atSevere(): API {
        return at(Level.SEVERE)
    }

    fun atSevere(function:API.()->Unit){
        function(atSevere())
    }

    /**
     * A convenience method for at([Level.ERROR])
     */
    fun atError():API{
        return at(Level.ERROR)
    }

    fun atError(function:API.()->Unit){
        function(atError())
    }

    /** A convenience method for at([Level.WARNING]).  */
    fun atWarning(): API {
        return at(Level.WARNING)
    }

    fun atWarning(function:API.()->Unit){
        function(atWarning())
    }

    /** A convenience method for at([Level.INFO]).  */
    fun atInfo(): API {
        return at(Level.INFO)
    }

    fun atInfo(function:API.()->Unit){
        function(atInfo())
    }

    fun atDebug():API{
        return at(Level.DEBUG)
    }

    fun atDebug(function:API.()->Unit){
        function(atDebug())
    }

    /** A convenience method for at([Level.CONFIG]).  */
    fun atConfig(): API {
        return at(Level.CONFIG)
    }

    fun atConfig(function:API.()->Unit){
        function(atConfig())
    }

    /** A convenience method for at([Level.FINE]).  */
    fun atTrace(): API {
        return at(Level.TRACE)
    }

    fun atTrace(function:API.()->Unit){
        function(atTrace())
    }

    /** A convenience method for at([Level.FINER]).  */
    fun atFiner(): API {
        return at(Level.FINER)
    }

    fun atFiner(function:API.()->Unit){
        function(atFiner())
    }

    /** A convenience method for at([Level.FINEST]).  */
    fun atFinest(): API {
        return at(Level.FINEST)
    }

    fun atFinest(function:API.()->Unit){
        function(atFinest())
    }

    /**
     * Returns whether the given level is enabled for this logger. Users wishing to guard code with a
     * check for "loggability" should use `logger.atLevel().isEnabled()` instead.
     */
    protected fun isLoggable(level: Level): Boolean {
        return backend.isLoggable(level)
    }

    fun getBackend():LogDelegate{
        return backend
    }

}
