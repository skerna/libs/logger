package io.skerna.common.logger

interface LogDelegate:LogLevels.Debug,
        LogLevels.Info,
        LogLevels.Warning,
        LogLevels.Error,
        LogLevels.Fatal,
        LogLevels.Trace{

    val isWarnEnabled: Boolean

    val isInfoEnabled: Boolean

    val isDebugEnabled: Boolean

    val isTraceEnabled: Boolean

    fun log(level:Level, message:String)

    fun log(level:Level,message: String, exception: Throwable)

    fun log(level:Level,message: String, vararg params:Any)

    fun log(level:Level,exception: Throwable, message: String, vararg params: Array<Any>)

    fun getLoggerName():String

    fun isLoggable(level: Level):Boolean
    /**
     * @return the underlying framework logger object, null in the default implementation
     */
    fun unwrap(): Any

    fun setExceptionHandler(exceptionHandler: ExceptionHandler)

    fun onException(exception: Exception)

}
