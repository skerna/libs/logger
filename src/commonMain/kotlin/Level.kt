package io.skerna.common.logger

enum class Level(val peso:Int) {
    INFO(0),
    WARNING(1),
    DEBUG(2),
    ERROR(3),
    CONFIG(4),
    TRACE(5),
    FINER(6),
    FINEST(7),
    SEVERE(8),
    ALL(Int.MAX_VALUE);
}
