package io.skerna.common.logger.ansi

import kotlin.jvm.JvmStatic

object Format {
    const val ANSI_RESET = "\u001B[0m"
    const val ANSI_UNDERLINE = "\u001B[4m"
    const val ANSI_ITALIC = "\u001B[3m"

    @JvmStatic
    fun underline(string: String) = format(string, ANSI_UNDERLINE)

    @JvmStatic
    fun italyc(string: String) = format(string, ANSI_ITALIC)

    private fun format(string: String, ansiString: String) = "$ansiString$string$ANSI_RESET"

}
