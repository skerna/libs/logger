package io.skerna.common.logger.ansi

import io.skerna.common.logger.ansi.Format

fun String.underline() = Format.underline(this)

fun String.italyc() = Format.italyc(this)

fun String.enclosed() = "($this)"
