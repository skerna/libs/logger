package io.skerna.common.logger

import kotlin.native.concurrent.ThreadLocal

class LoggerConfiguration {
    public var level: Level; private set
    public var infoEnabled: Boolean = true; private set
    public var debugEnabled: Boolean = false; private set
    public var warnEnabled: Boolean = false; private set
    public var traceEnabled: Boolean = false; private set
    private var exceptionHandler: ExceptionHandler? = null

    constructor(
        level: Level,
        infoEnabled: Boolean = true,
        debugEnabled: Boolean = false,
        warnEnabled: Boolean = false,
        traceEnabled: Boolean = false
    ) {
        this.level = level
        this.infoEnabled = infoEnabled
        this.debugEnabled = debugEnabled
        this.warnEnabled = warnEnabled
        this.traceEnabled = traceEnabled
    }

    fun setLevel(level: Level) = apply {
        this.level = level
    }

    fun enableDebug(debugEnabled: Boolean) = apply {
        this.debugEnabled = debugEnabled
    }

    fun enableInfo(infoEnabled: Boolean) = apply {
        this.infoEnabled = infoEnabled
    }

    fun enableWarn(warnEnabled: Boolean) = apply {
        this.warnEnabled = warnEnabled
    }

    fun enableTrace(traceEnabled: Boolean) = apply {
        this.traceEnabled = traceEnabled
    }

    fun enableAll() = apply {
        this.infoEnabled = true
        this.warnEnabled = true
        this.debugEnabled = true
        this.traceEnabled = true
    }

    fun isEnabledLevel(level: Level): Boolean {
        val enabledByWeight = this.level.peso < level.peso
        var enabledByLevel = false;
//        when(level){
//            Level.INFO ->  enabledByLevel = true
//            Level.DEBUG -> enabledByLevel = true
//            Level.WARNING -> enabledByLevel = true
//            Level.ERROR -> enabledByLevel = true
//            else -> enabledByLevel = false
//        }
        var enabled = enabledByWeight
        return enabled
    }
    @ThreadLocal
    public companion object {
        val instanceGlobalContext = LoggerConfiguration(level = Level.INFO)
    }
}
