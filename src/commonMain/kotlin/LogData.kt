package io.skerna.common.logger

import io.skerna.common.logger.Level


/**
 * A backend API for determining metadata associated with a log statement.
 *
 *
 * Some metadata is expected to be available for all log statements (such as the log level or a
 * timestamp) whereas other data is optional (class/method name for example). As well providing the
 * common logging metadata, customized loggers can choose to add arbitrary key/value pairs to the
 * log data. It is up to each logging backend implementation to decide how it interprets this data
 * using the hierarchical key. See [Metadata].
 */
interface LogData {

    fun level():Level

    fun loggerName():String

    fun message():String

    fun arguments():Array<Any>

    fun wasForced():Boolean

    fun exception():Throwable?

}
