package io.skerna.common.logger

abstract class AbstractLoggerDelegate(val name: String,
                                      val loggerConfiguration: LoggerConfiguration
) : LogDelegate, LogLevels.ALL {
    private var exceptionHandler: ExceptionHandler? = null


    override fun log(level: Level, message: String) {
        when (level) {
            Level.INFO -> info(message)
            Level.DEBUG -> debug(message)
            Level.ERROR -> error(message)
            Level.WARNING -> warn(message)
            Level.TRACE -> trace(message)
            Level.SEVERE -> fatal(message)
            else -> {
                throw IllegalStateException("Unknow level required")
            }
        }
    }

    override fun log(level: Level, message: String, exception: Throwable) {
        when (level) {
            Level.INFO -> info(message, exception)
            Level.DEBUG -> debug(message, exception)
            Level.ERROR -> error(message, exception)
            Level.WARNING -> warn(message, exception)
            Level.TRACE -> trace(message, exception)
            Level.SEVERE -> fatal(message, exception)
            else -> {
                throw IllegalStateException("Unknow level required")
            }
        }
    }

    override fun log(level: Level, message: String, vararg params: Any) {
        when (level) {
            Level.INFO -> info(message, *params)
            Level.DEBUG -> debug(message, *params)
            Level.ERROR -> error(message, *params)
            Level.WARNING -> warn(message, *params)
            Level.TRACE -> trace(message, *params)
            Level.SEVERE -> fatal(message)
            else -> {
                throw IllegalStateException("Unknow level required")
            }
        }
    }

    override fun log(level: Level, exception: Throwable, message: String, vararg params: Array<Any>) {
        when (level) {
            Level.INFO -> info(message, exception, *params)
            Level.DEBUG -> debug(message, exception, *params)
            Level.ERROR -> error(message, exception, *params)
            Level.WARNING -> warn(message, exception, *params)
            Level.TRACE -> trace(message, exception, *params)
            Level.SEVERE -> fatal(message, exception)
            else -> {
                throw IllegalStateException("Unknow level required")
            }
        }
    }

    override fun setExceptionHandler(exceptionHandler: ExceptionHandler) {
        this.exceptionHandler = exceptionHandler
    }

    override fun onException(exception: Exception) {
        exceptionHandler?.onException(exception)
    }

    override fun getLoggerName(): String {
        return this.name
    }

    override fun isLoggable(level: Level): Boolean {
        if (level == Level.INFO && isInfoEnabled) {
            return true
        } else if (level == Level.WARNING && isWarnEnabled) {
            return true
        } else if (level == Level.DEBUG && isDebugEnabled) {
            return true
        } else if (level == Level.TRACE && isTraceEnabled) {
            return true
        } else if (level == Level.ERROR) {
            return true;
        } else if (level == Level.SEVERE) {
            return true;
        } else if (level == Level.FINER) {
            return true;
        } else if (level == Level.FINEST) {
            return true;
        }
        return false
    }
}
