plugins {
    id("org.jetbrains.kotlin.multiplatform") version "1.4.21"
    id("io.skerna.libs.gradle.base") version "1.0.0"
}

fun version(moduleName:String) = project.property(moduleName)
fun kotlinx(module: String) = "org.jetbrains.kotlinx:kotlinx-$module:${version("kotlin_coroutines_version")}"

packageSpec{
    metadata{
        description("this library provide common logger multiplatform implementation.")
        developers{
            developer("Ronal Cardenas")
        }
        licences{
            licence("MIT")
        }
    }
}

kotlin {
    jvm {
        compilations.all {
            kotlinOptions.jvmTarget = "1.8"
        }
        testRuns["test"].executionTask.configure {
            useJUnit()
        }
    }
    js(LEGACY) {
        browser {
            testTask {
                useKarma {
                    useChromeHeadless()
                    webpackConfig.cssSupport.enabled = false
                }
            }
        }
    }
    val hostOs = System.getProperty("os.name")
    val isMingwX64 = hostOs.startsWith("Windows")
    val nativeTarget = when {
        hostOs == "Mac OS X" -> macosX64("native")
        hostOs == "Linux" -> linuxX64("native")
        isMingwX64 -> mingwX64("native")
        else -> throw GradleException("Host OS is not supported in Kotlin/Native.")
    }


    sourceSets {

        val commonMain by getting {
            dependencies {
                implementation(kotlin("stdlib-common"))
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
            }
        }
        val jvmMain by getting {
            dependencies {
                compileOnly("org.slf4j:slf4j-simple:${version("slf4j_version")}")
                compileOnly("org.slf4j:slf4j-api:${version("slf4j_version")}")
                compileOnly("org.apache.logging.log4j:log4j-api:${version("log4j_version")}")
                compileOnly("org.apache.logging.log4j:log4j-core:${version("log4j_version")}")
                compileOnly("io.vertx:vertx-core:${version("vertx_version")}")
            }
        }
        val jvmTest by getting {
            dependencies {
                implementation(kotlin("test-junit"))
                implementation("org.slf4j:slf4j-simple:${version("slf4j_version")}")
                implementation("org.slf4j:slf4j-api:${version("slf4j_version")}")
                implementation("org.apache.logging.log4j:log4j-api:${version("log4j_version")}")
                implementation("org.apache.logging.log4j:log4j-core:${version("log4j_version")}")
                implementation("io.vertx:vertx-core:${version("vertx_version")}")
            }
        }
        val jsMain by getting {
            dependencies {
            }
        }
        val jsTest by getting {
            dependencies {
                implementation(kotlin("test-js"))
            }
        }
        val nativeMain by getting{
            dependencies {
            }
        }
        val nativeTest by getting

    }
}
