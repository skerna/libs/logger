pluginManagement {
    repositories {
        mavenLocal()
        google()
        gradlePluginPortal()
        mavenCentral()
        jcenter()
    }
}

rootProject.name = "logger"

